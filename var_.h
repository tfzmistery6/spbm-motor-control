#include "stm32g474xx.h"

//буфер считываемой команды
#define RX_BUF_SIZE 80
char RX_FLAG_END_LINE = 0; //флаг принятия конца строки
char RXi; //указатель на элеммент массива на чтение
char RXc; //буфер для читаемого символа
char RX_BUF[RX_BUF_SIZE] = {'\0'}; //массив на чтение

uint16_t input_param[2]; //Буфер параметров команды принятой в USART1

char buffer_[10]; //Буфер для чисел, которые необходимо вывести в USART1

//Циклический буфер на отправку
#define TX_BUF_SIZE 256
int TX_BUF_empty_space; //Колчиство свободных байт в TX_BUF
int TXi_w; //Указатель куда писать байт в массив
int TXi_t; //Указатель откуда отправлять байт в массив
char TX_BUF[TX_BUF_SIZE] = {'\0'}; //Циклический массив для отправки данных по USART


//Переменные, ответственные за состояние защиты
uint8_t shield_state;

#define TRIGGERED 1
#define NORMAL 2

//Таблица степеней десятки
const uint32_t  pow10Table32[]=
{
    1000000000ul,
    100000000ul,
    10000000ul,
    1000000ul,
    100000ul,
    10000ul,
    1000ul,
    100ul,
    10ul,
    1ul
};

uint16_t tim6_counter;
uint16_t tim7_counter;

//Главные переменные для работы алгоримта
uint16_t sup_voltage; //Напряжение питания в единицах АЦП
uint16_t battery_voltage; //Напряжение питания в вольтах

_Bool battery_state;
#define ALRIGHT 1
#define ILL 0

uint8_t rotor_state;
#define STOP 0
#define ROTATE 1

uint8_t control_system_state;
#define END 0
#define BEGIN 1
#define SHORT_START_INIT 2
#define LONG_START_INIT 4
#define ERROR 5
#define MAIN_CYCLE 6
#define PAUSE 7

uint16_t rotor_speed;
uint16_t max_rotor_speed;
float max_rpm;

uint16_t potentiometer; //Сигнал с ручки потенциометра в единицах АЦП

uint16_t cmpa_val; //Скважность ШИМ (пересчитанная с ручки потенциометра)
uint16_t PWM_short; //Скважность короткого толчка на старте
uint16_t PWM_long; //Скважность длинного толчка на старте
uint32_t long_duration; //Длительность импульса PWM_long

//Состояния датчиков холла
_Bool HS1_state;
_Bool HS2_state;
_Bool HS3_state;

_Bool HS1_state_prev;
_Bool HS2_state_prev;
_Bool HS3_state_prev;

#define ARELOAD_VAL 255

#define CURRENT_THRESHOLD 162
