
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'spbm_motor_control' 
 * Target:  'Target 1' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32g4xx.h"

/* Keil.STM32CubeMX::Device:Startup:1.0.0 */
#define RTE_DEVICE_STARTUP_STM32G4XX    /* Device Startup for STM32G4 */


#endif /* RTE_COMPONENTS_H */
