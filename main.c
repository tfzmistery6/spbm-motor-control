/******************************************************************************
 * @file    main.c
 * @brief   Главный файл проекта системы управления однофазным мотором
 * @version v0.1
 * @date    21.04.2021
 * @author  Власовский Алексей Игоревич & Мануйлов Александр Владимирович
 * @note    АО "ОКБ МЭЛ" г.Калуга
 ******************************************************************************/

#include "stm32g474xx.h"
#include "SysTick_.h"
#include "var_.h"
#include "gpio_.h"
#include "usart_.h"
#include "tim_.h"
#include "adc_.h"
#include "exti_.h"

/*____________________PROTOTYPES____________________*/
void global_var_init(void); //дополнительная инициализация переменных
void NVIC_tuning(void); //Функция включения необходимых векторов прерываний
/*___Функциональные блоки___*/
void shield_init(void); //Инициализация защиты
void shield_reset(void); //Сброс защиты
void begin_PWM_calc(void); //Расчёт стартового ШИМа
/*___Коммуникации___*/
void clear_RXBuffer(void); //Очистка буфера принятых символов
static char *utoa_cycle_sub(uint32_t value, char *buffer); //Преобразование uint to ascii
int is_equal(char *string1, char *string2, int len); //Функция сравнения двух строк (замена strncmp)
int is_digit(char byte); //функция проверки является ли принятый байт цифрой в ascii кодировке
int get_param(char *buf); //выделение параметров из буфера
void USART1_TXBuf_append (char *buffer); //Отправка данных в USART1
void RXBuffer_Handler (void); //Обработчик буфера принятых символов после приёма CR

int main()
{
	//"включение FPU"
	SCB -> CPACR |= (3 << 20 | 3 << 22); //CP10 CP11 Full access
	
	global_var_init();
	gpio_init();
	usart1_init();
	SysTick_init();
	tim1_init();//Комплиментарный шим
	tim2_init();//ШИМ установки токов
	adc3_init();
	adc4_init();
	tim7_init();
	tim6_init();
	tim15_init();
	tim16_init();
	
	NVIC_EnableIRQ(USART1_IRQn);
	
	NVIC_EnableIRQ(ADC3_IRQn);
	NVIC_EnableIRQ(ADC4_IRQn);
	NVIC_EnableIRQ(EXTI15_10_IRQn);
	NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
			
	//Понизим приоритет прерывания TIM6 чтобы избежать коллизий
	//При ресете защиты
	NVIC_SetPriority(TIM6_DAC_IRQn, 2);
	
	SysTick_wait(16000000);//Ждём 1 секунду
	
	shield_init(); //попытка инициализировать плату защиты
	
	//Если состояние не ошибка
	if (shield_state != TRIGGERED)
	{
		//Запускаем прерывания для стабильной работы
		NVIC_EnableIRQ(TIM7_DAC_IRQn);
		exti_init();
		NVIC_EnableIRQ(EXTI9_5_IRQn);
	}
	else
	{
		//Если всё же ошибка, то запускаем таймер
		//перезапуска защиты и пытаемся запустить снова
		NVIC_EnableIRQ(TIM6_DAC_IRQn);
	}
	
	while(1);
}

/*_______________INTERRUPTS HANDLERS_______________*/

//Прерывание по переполнению таймера TIM7 (16 кГц)
void TIM7_DAC_IRQHandler (void)
{
	TIM7 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	
	HS1_state_prev = HS1_state;
	HS2_state_prev = HS2_state;
	HS3_state_prev = HS3_state;
	HS1_state = ((GPIOC -> IDR & GPIO_IDR_ID10) >> 10);
	HS2_state = ((GPIOC -> IDR & GPIO_IDR_ID9) >> 9);
	HS3_state = ((GPIOC -> IDR & GPIO_IDR_ID6) >> 6);
	battery_voltage = (int) (sup_voltage*125)/2048;
	cmpa_val = potentiometer/16;
	
	if (battery_voltage < 12)
	{
		battery_state = ILL;
	}
	else
	{
		battery_state = ALRIGHT;
	}
	
//	//Тестовый режим
//	if (cmpa_val > 12)
//	{
//		GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
//		GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
//		TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
//		TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
//		TIM1 -> CCR1 = ARELOAD_VAL - 64;
//		GPIOC -> ODR |= (GPIO_ODR_OD1); //Hi2 high
//	}
//	else
//	{
//		GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
//		GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
//		TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
//		TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
//		TIM1 -> CCR2 = ARELOAD_VAL - 64;
//		GPIOC -> ODR |= (GPIO_ODR_OD0); //Hi1 high
//	}
	
	//Основной свитч машины состояний системы управления
	switch (control_system_state)
	{
		case END:
			GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
			GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
			TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
			TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
		
			if (rotor_state == STOP && cmpa_val > 12)
			{
				rotor_speed = 65000;
				max_rotor_speed = 65000;
				control_system_state = BEGIN;
			}
		break;
		
		case BEGIN:
			begin_PWM_calc();
			
			USART1_TXBuf_append("Voltage ");
			USART1_TXBuf_append(utoa_cycle_sub(battery_voltage,buffer_));
			USART1_TXBuf_append("\n");
			USART1_TXBuf_append("PWM_short ");
			USART1_TXBuf_append(utoa_cycle_sub(PWM_short,buffer_));
			USART1_TXBuf_append("\n");
			USART1_TXBuf_append("PWM_long ");
			USART1_TXBuf_append(utoa_cycle_sub(PWM_long,buffer_));
			USART1_TXBuf_append("\n");
		
			control_system_state = SHORT_START_INIT;
		break;
		
		case SHORT_START_INIT:
			
			if (!HS1_state)
			{
				GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
				GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
				TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
				TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
				TIM1 -> CCR1 = ARELOAD_VAL - PWM_short;
				GPIOC -> ODR |= (GPIO_ODR_OD1); //Hi2 high
			}
			else
			{
				GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
				GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
				TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
				TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
				TIM1 -> CCR2 = ARELOAD_VAL - PWM_short;
				GPIOC -> ODR |= (GPIO_ODR_OD0); //Hi1 high
			}
			
			SysTick_wait(160000); //10 ms
			
			GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
			GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
			TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
			TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
			
			SysTick_wait(16000000);
			
			control_system_state = LONG_START_INIT;
		break;
			
		case LONG_START_INIT:
			
			if (!HS1_state)
			{
				GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
				GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
				TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
				TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
				TIM1 -> CCR1 = ARELOAD_VAL - PWM_long;
				GPIOC -> ODR |= (GPIO_ODR_OD1); //Hi2 high
			}
			else
			{
				GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
				GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
				TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
				TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
				TIM1 -> CCR2 = ARELOAD_VAL - PWM_long;
				GPIOC -> ODR |= (GPIO_ODR_OD0); //Hi1 high
			}
			
			SysTick_wait(long_duration);
			
			GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
			GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
			TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
			TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
			
			TIM16 -> CNT = 0; //отбросим таймер назад
			TIM15 -> CNT = 0;
			rotor_state = ROTATE; //сымитируем вращение
			
			control_system_state = MAIN_CYCLE;
		break;
			
		case MAIN_CYCLE:
			
			if (rotor_state == ROTATE)
			{
				if (HS1_state != HS1_state_prev)
				{
					if (rotor_speed < max_rotor_speed)
					{
						max_rotor_speed = rotor_speed;
					}
					
					if (!HS1_state)
					{
						GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
						GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
						TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
						TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
						TIM1 -> CCR1 = ARELOAD_VAL - cmpa_val;
						GPIOC -> ODR |= (GPIO_ODR_OD1); //Hi2 high
					}
					else
					{
						GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
						GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
						TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
						TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
						TIM1 -> CCR2 = ARELOAD_VAL - cmpa_val;
						GPIOC -> ODR |= (GPIO_ODR_OD0); //Hi1 high
					}
				}
				if (HS3_state != HS3_state_prev)
				{
					GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
					GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
					TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
					TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low
				}
			}
			else
			{
				control_system_state = PAUSE;
			}
			
		break;
			
		case PAUSE:
			USART1_TXBuf_append("Test END!\n");
			max_rpm = (60/(((float)(max_rotor_speed)/80000)*12));
			USART1_TXBuf_append("Max rotor speed: ");
			USART1_TXBuf_append(utoa_cycle_sub((int) max_rpm,buffer_));
			USART1_TXBuf_append("\n");
			control_system_state = ERROR;
		break;
		
		case ERROR:
			control_system_state = END;
		break;
	}
	
	tim7_counter++;
	if(tim7_counter >= 3200) //Этот if даёт 5 гц
	{
		tim7_counter = 0;
		
		GPIOD -> ODR ^= GPIO_ODR_OD2; //моргание светодиодом
	}
}

//Прерывание по переполнению таймера TIM6 (200 Гц)
void TIM6_DAC_IRQHandler (void)
{
	tim6_counter++;
	
	if (tim6_counter >= 600)//раз в 3 сек
	{
		tim6_counter = 0;
		USART1_TXBuf_append("\nSHIELD IS DOWN - TRYING TO RESET\n");
		shield_reset(); //попытка ресетнуть защиту
		
		//Если после попытки перезапуска всё хорошо
		if (shield_state == NORMAL)
		{
			//Восстанавливаем работу основного алгоримта и прерывание защиты
			NVIC_EnableIRQ(TIM7_DAC_IRQn);
			exti_init();
			NVIC_EnableIRQ(EXTI9_5_IRQn);
			//Выключаем это прерывание
			NVIC_DisableIRQ(TIM6_DAC_IRQn);
		}
	
	}
	
	//Сбрасываем в конце, чтобы не настакать прерывание сверху
	TIM6 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
}

//Прерывание USART1
void USART1_IRQHandler(void)
{
	if (USART1 -> ISR & USART_ISR_TXE) //data is transferred to the shift register
	{
		if (TXi_t != TXi_w)
		{
			USART1 -> TDR = TX_BUF[TXi_t];
			TXi_t ++;
			TX_BUF_empty_space++;
	
			if (TXi_t >= 256){ TXi_t = 0; }
		}
		else
		{
			USART1 -> CR1 &= ~USART_ISR_TXE; //Запретить прерывания на передачу
		}
	}
	
	//проверяем что был принят бит
	if (USART1 -> ISR & USART_ISR_RXNE) //флаг RXNE
	{
		
		RXc = USART1 -> RDR;
		RX_BUF[RXi] = RXc;
		RXi++;

		//Если не символ возврата каретки
		if (RXc != 13) {
			if (RXi > RX_BUF_SIZE-1) {
				clear_RXBuffer();
			}
		}
		else {
			RX_FLAG_END_LINE = 1;
		}
	}
}

//Прерывание ADC3 по флагу EOC
void ADC3_IRQHandler(void)
{
	sup_voltage = ADC3 -> DR;
}

//Прерывание ADC4 по флагу EOC
void ADC4_IRQHandler(void)
{
	potentiometer = ADC4 -> DR;
}

void EXTI9_5_IRQHandler(void)
{
	EXTI -> PR1 |= EXTI_PR1_PIF7; //сбрасываем прерывание перед самим прерыванием
	USART1_TXBuf_append("Shield falling edge detected!\n");
	
	shield_state = TRIGGERED; //Состояние - защита сработала

	GPIOC -> ODR &= ~(GPIO_ODR_OD0); //Hi1 low
	GPIOC -> ODR &= ~(GPIO_ODR_OD1); //Hi2 low
	TIM1 -> CCR1 = ARELOAD_VAL; //Low1 low
	TIM1 -> CCR2 = ARELOAD_VAL; //Low2 low

	//Если защита сработала нужно выключить прерывания стабильной работы
	NVIC_DisableIRQ(TIM7_DAC_IRQn); //Прерывание основного алгоритма
	NVIC_DisableIRQ(EXTI9_5_IRQn); //Само прерывание защиты
	
	//И нужно включить прерывание перезаупска защиты
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
	
	SysTick_wait(160);//Ждём 10 микросекунд
}

//Прерывание по первому датчику Холла
void EXTI15_10_IRQHandler(void)
{
	EXTI -> PR1 |= EXTI_PR1_PIF10; //сбрасываем прерывание перед самим прерыванием
	TIM16 -> CNT = 0; //отбросим таймер назад
	rotor_speed = TIM15 -> CNT; //измерим скорость вращения другим таймером
	TIM15 -> CNT = 0; //Сбросим в ноль
	rotor_state = ROTATE; //Фиксация факта вращения
}

//Прерывание по обновлению состояния ротора
void TIM1_UP_TIM16_IRQHandler(void)
{
	TIM16 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	rotor_state = STOP;
}

void Reset_Handler (void)
{
	main();
}

/*____________________FUNCTIONS____________________*/

/**
	* @brief  Инициализация переменных
  * @param  None
  * @retval None
  */
void global_var_init(void)
{
	clear_RXBuffer(); //очистим буфер перед работой
	
	TXi_t = 0;
	TXi_w = 0;
	TX_BUF_empty_space = 256; //При инициализациии буфер пуст
	
	tim6_counter = 0;
	tim7_counter = 0;
	
	shield_state = TRIGGERED; //по началу считаем защиту сработанной
	control_system_state = END; //Сначала считаем что всё выключено
	
	PWM_short = 0;
	PWM_long = 0;
}

//Функция включения необходимых векторов прерываний
void NVIC_tuning(void)
{
	NVIC_EnableIRQ(USART1_IRQn);
	//NVIC_EnableIRQ(TIM7_DAC_IRQn);
	NVIC_EnableIRQ(ADC3_IRQn);
	//NVIC_EnableIRQ(TIM6_DAC_IRQn);
};

/*___Функциональные блоки___*/

//Попытка запуска защиты
void shield_init(void)
{
	TIM2 -> CCR1 = 0; //0 в этом случае вечный 1 в режиме open drain
	//PB4 PB6 выключим на всякий случай
	GPIOB -> ODR |= (GPIO_ODR_OD4 | GPIO_ODR_OD6);
	SysTick_wait(160);//Ждём 10 микросекунд
	
	USART1_TXBuf_append("\nShield initialization...\n");
	
	if(GPIOC -> IDR & GPIO_IDR_ID7) //Проверяем Shield line IRQ
	{
		USART1_TXBuf_append("Shield error!\n");
		USART1_TXBuf_append("PC7 high due initialization!\n");
		shield_state = TRIGGERED;
		return;
	}
	else
	{
		USART1_TXBuf_append("PC7 low - check...\n");
	}
	
	USART1_TXBuf_append("Charging capacitors...\n");
	GPIOB -> ODR &= ~GPIO_ODR_OD4; //Просаживаем на 0 линию Lamp on
	SysTick_wait(16000000);//Ждём 1 секунду
	SysTick_wait(16000000);//Ждём 1 секунду
	GPIOB -> ODR |= GPIO_ODR_OD4; //Отпускаем линию Lamp on
	
	if (sup_voltage < 50) //Если напряжение питания не появилось
	{
		USART1_TXBuf_append("Shield error!\n");
		USART1_TXBuf_append("Supply voltage not detected!\n");
		USART1_TXBuf_append("Supply voltage: ");
		USART1_TXBuf_append(utoa_cycle_sub(sup_voltage,buffer_));
		USART1_TXBuf_append("\n");
		shield_state = TRIGGERED;
		return;
	}
	else
	{
		USART1_TXBuf_append("Supply voltage: ");
		USART1_TXBuf_append(utoa_cycle_sub(sup_voltage,buffer_));
		USART1_TXBuf_append("\n");
		USART1_TXBuf_append("Supply voltage - check...\n");
	}
	
	USART1_TXBuf_append("Adjusting the current...\n");
	TIM2 -> CCR1 = CURRENT_THRESHOLD; //Надо >50% поэтому 255 - 64
	SysTick_wait(8000000);//Ждём 0.5 секунды
	
	USART1_TXBuf_append("Reset shield...\n");
	GPIOB -> ODR &= ~GPIO_ODR_OD6; //Просаживаем на 0 линию Shield on
	SysTick_wait(1600);//Ждём 100 микросекунд
	GPIOB -> ODR |= GPIO_ODR_OD6; //Отпускаем линию Shield on
	
	SysTick_wait(16000);//Ждём 1000 микросекунд
	
	if(GPIOC -> IDR & GPIO_IDR_ID7) //Проверяем Shield line IRQ
	{
		USART1_TXBuf_append("Shield reset - check...\n");
		USART1_TXBuf_append("Continue system initialization.\n");
		shield_state = NORMAL;
	}
	else
	{
		USART1_TXBuf_append("Shield error!\n");
		USART1_TXBuf_append("PC7 low after reset!\n");
		shield_state = TRIGGERED;
		return;
	}
};

//Сброс защиты
void shield_reset(void)
{
	USART1_TXBuf_append("Charging capacitors...\n");
	GPIOB -> ODR &= ~GPIO_ODR_OD4; //Просаживаем на 0 линию Lamp on
	SysTick_wait(16000000);//Ждём 1 секунду
	SysTick_wait(16000000);//Ждём 1 секунду
	GPIOB -> ODR |= GPIO_ODR_OD4; //Отпускаем линию Lamp on
	
	if (sup_voltage < 50) //Если напряжение питания не появилось
	{
		USART1_TXBuf_append("Shield error!\n");
		USART1_TXBuf_append("Supply voltage not detected!\n");
		USART1_TXBuf_append("Supply voltage: ");
		USART1_TXBuf_append(utoa_cycle_sub(sup_voltage,buffer_));
		USART1_TXBuf_append("\n");
		shield_state = TRIGGERED;
		return;
	}
	else
	{
		USART1_TXBuf_append("Supply voltage: ");
		USART1_TXBuf_append(utoa_cycle_sub(sup_voltage,buffer_));
		USART1_TXBuf_append("\n");
		USART1_TXBuf_append("Supply voltage - check...\n");
	}
	
	USART1_TXBuf_append("Adjusting the current...\n");
	TIM2 -> CCR1 = CURRENT_THRESHOLD; //Надо >50% поэтому 255 - 64
	SysTick_wait(8000000);//Ждём 0.5 секунды
	
	USART1_TXBuf_append("Reset shield...\n");
	GPIOB -> ODR &= ~GPIO_ODR_OD6; //Просаживаем на 0 линию Shield on
	SysTick_wait(1600);//Ждём 100 микросекунд
	GPIOB -> ODR |= GPIO_ODR_OD6; //Отпускаем линию Shield on
	
	SysTick_wait(16000);//Ждём 1000 микросекунд
	if(GPIOC -> IDR & GPIO_IDR_ID7) //Проверяем Shield line IRQ
	{
		USART1_TXBuf_append("Shield reset - check...\n");
		USART1_TXBuf_append("Continue system initialization.\n");
		shield_state = NORMAL;
	}
	else
	{
		USART1_TXBuf_append("Shield error!\n");
		USART1_TXBuf_append("PC7 low after reset!\n");
		shield_state = TRIGGERED;
		return;
	}
}

//Рассчёт значений стартовых толчков через линейную аппроксимацию
void begin_PWM_calc(void)
{
	//расчёт скважности импульсов
	if ((battery_voltage >= 12) && (battery_voltage < 36))
	{
		PWM_short = (int) ((-0.625 * ((float) battery_voltage)) + 67.5);
		PWM_long = (int) ((-2.08 * ((float) battery_voltage)) + 175);
	}
	else if ((battery_voltage >= 36) && (battery_voltage < 108))
	{
		PWM_short = (int) ((-0.42 * ((float) battery_voltage)) + 60);
		PWM_long = (int) ((-0.9 * ((float) battery_voltage)) + 132.5);
	}
	else if ((battery_voltage >= 108) && (battery_voltage < 180))
	{
		PWM_short = (int) ((-0.11 * ((float) battery_voltage)) + 27);
		PWM_long = (int) ((-0.28 * ((float) battery_voltage)) + 65);
	}
	else if ((battery_voltage >= 108) && (battery_voltage < 180))
	{
		PWM_short = (int) ((-0.056 * ((float) battery_voltage)) + 17);
		PWM_long = (int) ((-0.14 * ((float) battery_voltage)) + 40);
	}
	
	//Уточнение длительности long импульса
	if (battery_voltage < 40) long_duration = 1600000; // 100 ms
	if ((battery_voltage >= 40) && (battery_voltage < 100)) long_duration = 1600000;// 100 ms
	if ((battery_voltage >= 100) && (battery_voltage < 160)) long_duration = 1920000;//120 ms
	if ((battery_voltage >= 160) && (battery_voltage < 220)) long_duration = 2400000;//150 ms
}

/*___Коммуникации___*/
//очистка буфера приёмника
void clear_RXBuffer(void) {
	for (RXi=0; RXi<RX_BUF_SIZE; RXi++)
		RX_BUF[RXi] = '\0';
	RXi = 0;
	
	input_param[0] = 0;
	input_param[1] = 0;
}
/**
	* @brief  Запись данных в циклический буфер на отправку через USART
	* @param  Массив данных, предназначенных на отправку
  * @retval None
  */
void USART1_TXBuf_append (char *buffer)
{
	//Пока в буфере есть данные
	while (*buffer)
	{
		TX_BUF[TXi_w] = *buffer++;
		TXi_w++;
		TX_BUF_empty_space--;
		
		if (TXi_w >= 256){ TXi_w = 0; }
	}
	
	USART1 -> CR1 |= (1 << 7); //разрешить прерывание на передачу
	
	//блок ниже вроде как не нужен, потому что флаг TXE не может быть сброшен никогда
	/*
	//Если USART ничего не отправляет, то необходимо отправить один бит
	if (USART1 -> ISR & (1 << 7)) //data is transferred to the shift register
	{
		USART1 -> TDR = TX_BUF[TXi_t];
		TXi_t ++;
		TX_BUF_empty_space++;
	}
	*/
	
	if (TXi_t >= 256){ TXi_t = 0; }
}

/**
	* @brief  Перевод числа в аски символы методом циклического вычитания
	* @param  value - число, которое необходимо преобразовать
	*					*buffer - адрес массив, куда писать результат
  * @retval Возвращает буфер с записанным результататом преобразования
  */
static char * utoa_cycle_sub(uint32_t value, char *buffer)
{
   if(value == 0)
   {
      buffer[0] = '0';
      buffer[1] = 0;
      return buffer;
   }
   char *ptr = buffer;
   uint8_t i = 0;
   do
   {
      uint32_t pow10 = pow10Table32[i++];
      uint8_t count = 0;
      while(value >= pow10)
      {
         count ++;
         value -= pow10;
      }
      *ptr++ = count + '0';
   }while(i < 10);
   *ptr = 0;
	// удаляем ведущие нули
		while(*buffer == '0')
		{
		++buffer;
		}
   return buffer;
}

/**
	* @brief  Функция проверяет по таблице ascii является ли байт цифрой
	* @param  byte - первая строка
	* @retval Возвращает 1, если байт является цифрой
  */
int is_digit(char byte)
{
	if ((byte == 0x30) | (byte == 0x31) | (byte == 0x32) | (byte == 0x33) |\
		(byte == 0x34) | (byte == 0x35) | (byte == 0x36) | (byte == 0x37) |\
	(byte == 0x38) | (byte == 0x39))
	{
		return 1;
	}
	else
	{
		return 0;
	}
};

/**
	* @brief  Функция сравнения двух строк (замена strncmp)
	* @param  string1 - первая строка
	*					string2 - вторая строка
	*					len - длинна сравнения (количество байт)
	* @retval Возвращает 0, если строки равны
  */
int is_equal(char *string1, char *string2, int len)
{
	while(len --> 0)
	{
		if(*string1++ != *string2++)
		{
			return 1;
		}
	}
	return 0;
};

/**
	* @brief  Поиск в буфере чисел как параметров команды
	* @param  *buf - адрес массива на приём
	* @retval Возвращает 0 при возникновении ошибки
  */
int get_param(char *buf)
{
	uint8_t num_of_params = 2;
	uint8_t i = 0;
	uint8_t pos = 0;
	int value = 0;
	
	//Дойдём до первого пробела
	while(buf[pos] != ' ')
	{
		pos++;
		//Если прошли весь буфер и не нашли пробела
		if (pos >= RX_BUF_SIZE)
		{
			//Выходим из функции
			return 0;
		}
	}
	
	//Начиная со следующего символа начнём вычислять параметры
	while(buf[++pos] != '\r')
	{
		if (!(is_digit(buf[pos]) | (buf[pos] == ' ')))
		{
			USART1_TXBuf_append("Read error!\r");
			clear_RXBuffer();
			return 0;
		}
		
		if (buf[pos] != ' ')
		{
			value = value*10 + (buf[pos] - 48);
		}
		else
		{
			input_param[i] = value;
			i++;
			value = 0;
			if (i > num_of_params -1)
			{
				USART1_TXBuf_append("Read error!\r");
				clear_RXBuffer();
				return 0;
			}
		}
	}
	
	//Запишем параметр, который будет перед концом строки
	input_param[i] = value;
	
	return 1;
};

//Обработка буфера приёма
void RXBuffer_Handler (void)
{
	
	//Если что-то было считано в буфер команды
	if (RX_FLAG_END_LINE == 1) {
		
		//Вычислим параметры у команды
		get_param(RX_BUF);
		
		// Reset END_LINE Flag
		RX_FLAG_END_LINE = 0;
		
		//Включить кулер
		if (is_equal(RX_BUF, "on\r", 3) == 0) {
			USART1_TXBuf_append("Fan ON\r");
		}
				
		clear_RXBuffer();
	}
}
