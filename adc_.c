#include "stm32g474xx.h"

void adc3_init(void)
{
/*___________ADC3___________*/
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PB1 - ADC3_In1 - Входное напряжение
	*/
	//Включим тактирование GPIO
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
	
	//Включим тактирование ADC3 и ADC4
	RCC -> AHB2ENR |= RCC_AHB2ENR_ADC345EN;
	ADC345_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC3 -> CR = 0;//Сбросим регистр CR
	ADC3 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	//Просто подёргаем битом внутри цикла
	for (int i = 0; i <= 120; i++){GPIOB -> ODR ^= GPIO_ODR_OD11;}
	GPIOB -> ODR &= ~(1 << 11);
	
	ADC3 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC3 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC3 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC3 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC3
	ADC3 -> CFGR |= (1 << 13); //continuous mode
	ADC3 -> SQR1 |= (0x0 << 0); // L = 1 преобразований (прим. 0000 == 1 пробразование)
	ADC3 -> SQR1 |= (0x1 << 6); //SQ1 = In1

	ADC3 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks

	ADC3 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	
	ADC3 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC3_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC3_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC3 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
};

void adc4_init(void)
{
/*___________ADC4___________*/
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PB15 - ADC4_In5 - Ручка потенциометра
	*/
	//Включим тактирование GPIO
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
	
	//Включим тактирование ADC3 и ADC4
	RCC -> AHB2ENR |= RCC_AHB2ENR_ADC345EN;
	ADC345_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC4 -> CR = 0;//Сбросим регистр CR
	ADC4 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	//Просто подёргаем битом внутри цикла
	for (int i = 0; i <= 120; i++){GPIOB -> ODR ^= GPIO_ODR_OD11;}
	GPIOB -> ODR &= ~(1 << 11);
	
	ADC4 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC4 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC4 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC4 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC3
	ADC4 -> CFGR |= (1 << 13); //continuous mode
	ADC4 -> SQR1 |= (0x0 << 0); // L = 1 преобразований (прим. 0000 == 1 пробразование)
	ADC4 -> SQR1 |= (0x5 << 6); //SQ1 = In5

	ADC4 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks

	ADC4 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	
	ADC4 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC3_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC3_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC4 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
};