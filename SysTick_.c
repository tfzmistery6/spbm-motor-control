#include "stm32g474xx.h"
#include "SysTick_.h"

/** 
 * @brief  Инициализация SysTick для формирования временных задержек
 * @param  none.
 * @return none.
 */
void SysTick_init(void)
{
	SysTick->CTRL = 0x00; //disable SysTick during setup
	SysTick->CTRL |= (1<<2); //freq from core`s freq
}

/** 
 * @brief  Временная задержка
 * @param  delay - количество тиков
 * @return none.
 * Обычно для калибровки данной задержки лучше написать тест
 * т.к. в некоторых реализациях встречаются баги (по моему опыту)
 */
void SysTick_wait(uint32_t delay)
{
	//На один такт приходится 62.5 наносекунд
	SysTick->LOAD = delay -1; //load with the number of ticks to wait
	SysTick->VAL = 0; //clear timer value
	SysTick->CTRL |= (1 << 0); //включить таймер
	while(!(SysTick->CTRL & (1<<16))); //wait until counterflag`s raise
	SysTick->CTRL &= ~(1 << 0); //выключить таймер
}
